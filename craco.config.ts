import cracoAlias from "craco-alias";
import cracoLess from "craco-less";

module.exports = {
  plugins: [
    {
      plugin: cracoLess,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              "@font-size-base": "14px",
              "@font-family": "Open Sauce Sans, Segoe UI, Roboto, sans-serif",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
    {
      plugin: cracoAlias,
      options: {
        source: "tsconfig",
        baseUrl: "./src",
        tsConfigPath: "./tsconfig.json",
      },
    },
  ],
};

export {};
