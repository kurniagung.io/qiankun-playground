import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import {
  registerMicroApps,
  start,
  setDefaultMountApp,
  prefetchApps,
  addGlobalUncaughtErrorHandler,
} from "qiankun";

import App from "./App";
import reportWebVitals from "./reportWebVitals";

import "./index.less";

registerMicroApps([
  {
    name: "mf-react",
    entry: "//localhost:8000",
    container: "#container",
    activeRule: "/react-app",
  },
  {
    name: "mf-react-2",
    entry: "//localhost:8080",
    container: "#container",
    activeRule: "/react-app-2",
  },
  {
    name: "react-craco",
    entry: "//localhost:8100",
    container: "#container",
    activeRule: "/react-craco",
  },
]);

prefetchApps([{ name: "mf-react", entry: "//localhost:8000" }]);
prefetchApps([{ name: "mf-react-2", entry: "//localhost:8080" }]);
start();
setDefaultMountApp("/react-app-1");
addGlobalUncaughtErrorHandler((event) => console.log(event));

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
