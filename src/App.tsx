import { Layout, Menu, MenuProps } from "antd";
import { Link } from "react-router-dom";
import { Atom } from "phosphor-react";

const { Header, Content, Footer } = Layout;

type MenuItem = Required<MenuProps>["items"][number];

export default function App(): JSX.Element {
  function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: "group"
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label,
      type,
    } as MenuItem;
  }

  const items = [
    getItem(
      <Link to="/react-app">React application 1</Link>,
      "1",
      <Atom size={14} />
    ),
    getItem(
      <Link to="/react-app-2">React application 2</Link>,
      "2",
      <Atom size={14} />
    ),
    getItem(
      <Link to="/react-craco">React + Craco</Link>,
      "3",
      <Atom size={14} />
    ),
  ];

  return (
    <Layout>
      <Header>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["2"]}
          items={items}
        />
      </Header>
      <Layout className="site-layout">
        <Content>
          <div
            id="container"
            style={{ minHeight: 500, background: "#fff" }}
          ></div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          This Project 2022 Created by Agung Kurniadi
        </Footer>
      </Layout>
    </Layout>
  );
}
