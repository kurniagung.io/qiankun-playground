import "./public-path";
import React from "react";
import ReactDOM from "react-dom/client";
import { unmountComponentAtNode } from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import App from "./App";
import reportWebVitals from "./reportWebVitals";

declare global {
  interface Window {
    __POWERED_BY_QIANKUN__: any;
  }
}

const render = (props: any): any => {
  const { container } = props;
  const root = ReactDOM.createRoot(
    container
      ? container.querySelector("#root")
      : (document.querySelector("#root") as HTMLElement)
  );

  root.render(
    <React.StrictMode>
      <Router basename={window.__POWERED_BY_QIANKUN__ ? "/react-craco" : "/"}>
        <App />
      </Router>
    </React.StrictMode>
  );
};

const storeTest = (props: any): void => {
  props.onGlobalStateChange(
    (value: any, prev: any) =>
      console.log(`[onGlobalStateChange - ${props.name}]:`, value, prev),
    true
  );
  props.setGlobalState({
    ignore: props.name,
    user: {
      name: props.name,
    },
  });
};

if (!window.__POWERED_BY_QIANKUN__) {
  render({});
}

export async function bootstrap() {
  console.log(`[react18] react app bootstraped`);
}

export async function mount(props: any) {
  console.log("[react18] props from main framework", props);
  storeTest(props);
  render(props);
}

export async function unmount(props: any) {
  const { container } = props;
  unmountComponentAtNode(
    container
      ? container.querySelector("#root")
      : document.querySelector("#root")
  );
}

// const root = ReactDOM.createRoot(
//   document.getElementById("root") as HTMLElement
// );
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

reportWebVitals();
