import cracoAlias from "craco-alias";
import { name } from "./package.json";

module.exports = {
  webpack: {
    output: {
      library: `${name}-[name]`,
      target: "umd",
      chunkLoadingGlobal: `webpackJsonp_${name}`,
    },
    devServer: {
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
      historyApiFallback: true,
      hot: false,
      injectClient: false,
      liveReload: false,
      watchContentBase: false,
    },
  },
  plugins: [
    {
      plugin: cracoAlias,
      options: {
        source: "tsconfig",
        baseUrl: "./src",
        tsConfigPath: "./tsconfig.json",
      },
    },
  ],
};

export {};
